package ru.otp.mq.test;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.otpbank.idms.mq.messagehandler.MQMessageListener;
import com.otpbank.idms.server.MqConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Gateway {

	private static final Logger logger = LoggerFactory.getLogger(Gateway.class);

	public static void main(String[] args) throws JMSException, IOException, InterruptedException {
		MQQueueConnectionFactory connectionFactory = getMqQueueConnectionFactory();

		DefaultMessageListenerContainer listener = new DefaultMessageListenerContainer();
		listener.setConnectionFactory(connectionFactory);
		listener.setConcurrency("1");
		listener.setRecoveryInterval(60000);
		listener.setDestination(new MQQueue("MQ_E2E_OPTY_NS_SIB_RESPONSE.CLONE"));
		listener.setMessageListener(new MQMessageListener("mq1"));
		listener.initialize();
		listener.start();

		while (true) {
			JmsTemplate template = new JmsTemplate(connectionFactory);
			template.send("MQ_E2E_OPTY_NS_SIB_REQUEST", new MessageCreator() {
				@Override
				public Message createMessage(final Session session) throws JMSException {
					String messageId = UUID.randomUUID().toString();

					logger.warn("Sending message with id: " + messageId);
					return session.createTextMessage(
							"<mes:Message xmlns=\"http://otpbank.ru/Fullapp\" xmlns:mes=\"http://otpbank.ru/Message\" xmlns:ns1=\"http://otpbank.ru/Message/UniversalOpty\">\n" +
									"\t<MessageHeader>\n" +
									"\t\t<MessageDate>2020-03-26T16:19:09.000767</MessageDate>\n" +
									"\t\t<ProcessID>LENDING_ONLINE</ProcessID>\n" +
									"\t\t<MessageType>NetSmartOptyLoan</MessageType>\n" +
									"\t\t<MessageID>" + messageId + " </MessageID>\n" +
									"\t\t<IntegrationID>" + messageId + "</IntegrationID>\n" +
									"\t\t<InitiatorSystem>LENDING_ONLINE</InitiatorSystem>\n" +
									"\t\t<SourceSystem>LENDING_ONLINE</SourceSystem>\n" +
									"\t\t<TargetSystemList>\n" +
									"\t\t\t<TargetSystem>SIEBEL</TargetSystem>\n" +
									"\t\t</TargetSystemList>\n" +
									"\t\t<Version>S-1</Version>\n" +
									"\t</MessageHeader>\n" +
									"\t<MessageBody>\n" +
									"\t\t<UniversalOptyRequest>\n" +
									"\t\t\t<ns1:System Version=\"S-1\"/>\n" +
									"\t\t\t<ns1:Operations>\n" +
									"\t\t\t\t<ns1:Operation Name=\"New\" Sequence=\"1\"/>\n" +
									"\t\t\t</ns1:Operations>\n" +
									"\t\t\t<ns1:Data>\n" +
									"\t\t\t\t<ns1:Opportunity Amount=\"320000\" CityOfReceiving=\"Москва\" CreationChannel=\"Website\" Currency=\"RUR\" MonthlyIncome=\"500000\" PaymentOnCurrentLoans=\"0\" Phone=\"+79776991249\" ProductType=\"Loan\" RegionOfReceiving=\"Москва\" RequestedAmount=\"320000\" Term=\"24\" TermsOfApplicationFlg=\"Y\">\n" +
									"\t\t\t\t\t<ns1:Insurances/>\n" +
									"\t\t\t\t</ns1:Opportunity>\n" +
									"\t\t\t\t<ns1:Client BirthCountry=\"Россия\" BirthDate=\"11/11/1991\" CellPhone=\"+79776991249\" Email=\"mail@mail.ru\" FirstName=\"Нагрузочно\" Gender=\"F\" LastName=\"Оковый\" MiddleName=\"Ивановна\" MobilePhone=\"+79776991249\" NoCourtFlg=\"True\"/>\n" +
									"\t\t\t</ns1:Data>\n" +
									"\t\t\t<ns1:TechFields APIFlg=\"N\" BrowserFamily=\"Firefox\" BrowserVersion=\"40.0\" DateTimeUA=\"03/26/2020 16:19:07\" DeviceType=\"Other\" IPCreate=\"192.168.57.196\" Os=\"Windows 7\" OsVersion=\"None.None\" OtpbankUtmCookie=\"\" Referrer=\"http://test-acc.otpbank.ru/public/e2e/index.html?_test_data=empty.values\" URL=\"http://test-acc.otpbank.ru/api/e2e/form/init/\" UserAgent=\"Mozilla/5.0 (Windows NT 6.1; rv:40.0) Gecko/20100101 Firefox/40.0\" UtmMedium=\"direct\"/>\n" +
									"\t\t</UniversalOptyRequest>\n" +
									"\t</MessageBody>\n" +
									"</mes:Message>");
				}
			});
			TimeUnit.SECONDS.sleep(10);
		}
	}

	private static MQQueueConnectionFactory getMqQueueConnectionFactory() throws JMSException {
		MQQueueConnectionFactory connectionFactory = new MQQueueConnectionFactory();

		connectionFactory.setHostName("192.168.63.201");
		connectionFactory.setPort(1401);
		connectionFactory.setQueueManager("WMB01PQM");
		connectionFactory.setChannel("NETSMART.MQ.SVRCONN");
		connectionFactory.setClientReconnectOptions(WMQConstants.WMQ_CLIENT_RECONNECT_DISABLED);
		connectionFactory.setStringProperty(WMQConstants.USERID, "netsmart");
		connectionFactory.setStringProperty(WMQConstants.PASSWORD, "netsmart");
		connectionFactory.setTransportType(WMQConstants.WMQ_CM_CLIENT);

		return connectionFactory;
	}
}
