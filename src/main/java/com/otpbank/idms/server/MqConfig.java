package com.otpbank.idms.server;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.otpbank.idms.mq.messagehandler.MQMessageListener;
import com.otpbank.idms.mq.messagehandler.MessageProcessor;
import com.otpbank.idms.mq.messagehandler.MessageSender;
import com.otpbank.idms.util.PasswordCipher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.jms.JMSException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Created by NikitushkinVG on 31.01.2017.
 */

@Configuration
public class MqConfig {

    private static final Logger logger = LoggerFactory.getLogger(MqConfig.class);
    private List<DefaultMessageListenerContainer> listeners = new ArrayList<>();
    private PasswordCipher decoder = new PasswordCipher();

    @Bean(name = "properties")
    Properties properties() throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
        if (inputStream == null) {
            throw new FileNotFoundException("application.properties not found in the classpath");
        }
        properties.load(inputStream);

        return properties;
    }

    @Bean(destroyMethod = "destroy")
    ListenerCloser listenerCloser() {
        return new ListenerCloser(listeners);
    }

    @Bean(name = "marshaller")
    Jaxb2Marshaller marshaller()  {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
      //  marshaller.setClassesToBeBound(RequestType.class, ResponseType.class);
        return marshaller;
    }

    @Bean(name = "marshallingMessageConverter")
    @Autowired
    MarshallingMessageConverter marshallingMessageConverter(Marshaller marshaller) {
        MarshallingMessageConverter marshallingMessageConverter = new MarshallingMessageConverter();
        marshallingMessageConverter.setTargetType(MessageType.TEXT);
        marshallingMessageConverter.setMarshaller(marshaller);
        marshallingMessageConverter.setUnmarshaller((Unmarshaller) marshaller);
        System.out.println();


        return marshallingMessageConverter;
    }


    @Bean(name = "messageWorkers")
    @Autowired
    Map<String, MessageSender> messageWorkers(Properties properties, MarshallingMessageConverter marshallingMessageConverter) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        Map<String, MessageSender> messageWorkers = new HashMap<String, MessageSender>();
        MessageProcessor.setMessageWorkers(messageWorkers);
        MessageProcessor.setMessageConverter(marshallingMessageConverter);

        String[] activeMqs = properties.getProperty("activeMqs").split(",");
        for (String s : activeMqs) {
            s = s.trim();
            String queueManager = properties.getProperty(s + "." + "queueManager");
            String hostName = properties.getProperty(s + "." + "hostName");
            String port = properties.getProperty(s + "." + "port");
            String channel = properties.getProperty(s + "." + "channel");
            String incomeQueue = properties.getProperty(s + "." + "incomeQueue");
            String outputQueue = properties.getProperty(s + "." + "outputQueue");
            String username = properties.getProperty(s + "." + "username");
            String password = decoder.decode(properties.getProperty(s + "." + "password"));
            String concurrency = properties.getProperty(s + "." + "concurrency");
            String expiration = properties.getProperty(s + "." + "expiration");

            try {
                MQQueueConnectionFactory connectionFactory = new MQQueueConnectionFactory();
                connectionFactory.setQueueManager(queueManager);
                connectionFactory.setHostName(hostName);
                connectionFactory.setPort(Integer.parseInt(port));
                connectionFactory.setChannel(channel);
                connectionFactory.setTransportType(1);

                UserCredentialsConnectionFactoryAdapter verifiedConnectionFactory = new UserCredentialsConnectionFactoryAdapter();
                verifiedConnectionFactory.setUsername(username);
                verifiedConnectionFactory.setPassword(password);
                verifiedConnectionFactory.setTargetConnectionFactory(connectionFactory);

                JmsTemplate jmsTemplate = new JmsTemplate();
                jmsTemplate.setConnectionFactory(verifiedConnectionFactory);
                MQQueue mqQueueOutput = new MQQueue(outputQueue);
                mqQueueOutput.setTargetClient(1);
                mqQueueOutput.setExpiry(Long.parseLong(expiration));
                jmsTemplate.setDefaultDestination(mqQueueOutput);

                MessageSender messageSender = new MessageSender();
                messageSender.setConnectionFactory(verifiedConnectionFactory);
                messageSender.setJmsTemplate(jmsTemplate);
                messageSender.setMessageConverter(marshallingMessageConverter);
                messageWorkers.put(s, messageSender);

                DefaultMessageListenerContainer listener = new DefaultMessageListenerContainer();
                listener.setConnectionFactory(verifiedConnectionFactory);
                listener.setConcurrency(concurrency);
                listener.setRecoveryInterval(60000);
                listener.setDestination(new MQQueue(incomeQueue));
                listener.setMessageListener(new MQMessageListener(s));
                listeners.add(listener);
                listener.initialize();
                listener.start();

            } catch (JMSException e) {
                logger.error("Error in " + s + " init.", e);
            }
        }
        return messageWorkers;
    }


    @Bean
    CustomValidationEventHandler eventHandler() {
        return new CustomValidationEventHandler();
    }

}
