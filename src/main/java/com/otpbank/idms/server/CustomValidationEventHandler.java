package com.otpbank.idms.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

/**
 * Created by NikitushkinVG on 02.02.2017.
 */
public class CustomValidationEventHandler implements ValidationEventHandler {
    private static final Logger logger = LoggerFactory.getLogger(CustomValidationEventHandler.class);
    @Override
    public boolean handleEvent(ValidationEvent event) {
        logger.debug(event.getMessage());
        return true;
    }
}
