package com.otpbank.idms.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by NikitushkinVG on 07.02.2017.
 */
@Component
public class ListenerCloser implements DisposableBean {
    private static final Logger logger = LoggerFactory.getLogger(ListenerCloser.class);

    private List<DefaultMessageListenerContainer> listeners;

    public ListenerCloser(List<DefaultMessageListenerContainer> listeners) {
        logger.info("ListenerCloser init.");
        this.listeners = listeners;
    }

    @Override
    public void destroy() throws Exception {
        for (DefaultMessageListenerContainer listener : listeners) {
            try {
                logger.info("Closing " + listener + " listener.");
                listener.shutdown();
                listener.destroy();
            } catch (Exception e) {
                logger.error("Error while closing mq " + listener, e);
            }
        }
    }
}
