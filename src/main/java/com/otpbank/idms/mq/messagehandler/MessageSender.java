package com.otpbank.idms.mq.messagehandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MarshallingMessageConverter;

import javax.jms.*;

public class MessageSender {

    private static final Logger logger = LoggerFactory.getLogger(MessageSender.class);

    private ConnectionFactory connectionFactory;

    private MarshallingMessageConverter messageConverter;

    private JmsTemplate jmsTemplate;


    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }


    public Boolean sendMessage(ResponseType response, String messageId) {
        boolean sendResult = true;
        Connection jmsConnection = null;
        try {
            jmsConnection = connectionFactory.createConnection();
            jmsConnection.start();

            Session session = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Message result = messageConverter.toMessage(response, session);

            result.setJMSCorrelationID(messageId);
            if (logger.isDebugEnabled()) {
                try {
                    logger.debug(((TextMessage) result).getText());
                } catch (JMSException e) {
                    logger.debug("Not TextMessage", e);
                }
            }

            jmsTemplate.convertAndSend(result);
        } catch (JMSException e) {
            logger.error("Failed to send message", e);
            sendResult = false;
        } finally {
            try {
                if (jmsConnection != null) {
                    jmsConnection.stop();
                    jmsConnection.close();
                    jmsConnection = null;
                }
            } catch (JMSException e) {
                logger.error("Failed to close connection", e);
            }

        }
        return sendResult;

    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public MarshallingMessageConverter getMessageConverter() {
        return messageConverter;
    }

    public void setMessageConverter(MarshallingMessageConverter messageConverter) {
        this.messageConverter = messageConverter;
    }

    public JmsTemplate getJmsTemplate() {
        return jmsTemplate;
    }

    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }


}
