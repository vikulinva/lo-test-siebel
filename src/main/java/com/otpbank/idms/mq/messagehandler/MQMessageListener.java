package com.otpbank.idms.mq.messagehandler;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.listener.SessionAwareMessageListener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

public class MQMessageListener implements SessionAwareMessageListener {


    private static final Logger logger = LoggerFactory.getLogger(MQMessageListener.class);
    private String sourceServer;

    public MQMessageListener(String sourceServer) {
        logger.debug(sourceServer + " MQMessageListener initialization...");
        this.sourceServer = sourceServer;
    }

    public void onMessage(final Message message, final Session session) {

            try {
                logger.debug("Income message from " + sourceServer + " server: " + message.getJMSMessageID());
                logger.debug(((TextMessage) message).getText());
            } catch (JMSException e) {
                logger.debug("Not TextMessage", e);
            }

//        MessageProcessor.processIncomeMessage(message, session, sourceServer);
    }

}
