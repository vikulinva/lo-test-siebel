package com.otpbank.idms.mq.messagehandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.converter.MessageConversionException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.util.Map;

public class MessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(MessageProcessor.class);
    private static MarshallingMessageConverter messageConverter;

    private static Map<String, MessageSender> messageWorkers;

  public static void processIncomeMessage(Message message, Session session, String sourceServer) {
        RequestType request = null;

        String messageId = null;

        try {

            messageId = message.getJMSMessageID();

            request = (RequestType) messageConverter.fromMessage(message);

            ResponseType response = new ResponseType();

            sendMessage(response, messageId, sourceServer);

        } catch (MessageConversionException e) {
            logger.error("Message conversion error", e);
        } catch (JMSException e) {
            String errorMessage = "Uncaught JMS exception while processing message: ";
            logger.error(errorMessage, e);
        }

    }

    public static MarshallingMessageConverter getMessageConverter() {
        return messageConverter;
    }

    public static void setMessageWorkers(Map<String, MessageSender> messageWorkers) {
        MessageProcessor.messageWorkers = messageWorkers;
    }


    public static void setMessageConverter(MarshallingMessageConverter messageConverter) {
        MessageProcessor.messageConverter = messageConverter;
    }


    private static void sendMessage(ResponseType response, String messageId, String sourceServer) {
        if (sourceServer == null) {
            sourceServer = "1";
        }

        if (!sourceServer.startsWith("mq")) {
            sourceServer = "mq" + sourceServer;
        }
        MessageSender currentSender = messageWorkers.get(sourceServer);

        if (currentSender != null) {
            logger.debug("sendNewPosMqResponse to server " + sourceServer + " (" + currentSender + ")");
            try {
                boolean result = currentSender.sendMessage(response, messageId);
            } catch (Exception e) {
                logger.error("Cannot send response to message " + messageId, e);
            }
        } else {
            logger.error("Specified server [" + sourceServer + "] not configurated in application.properties");
        }
    }

}
