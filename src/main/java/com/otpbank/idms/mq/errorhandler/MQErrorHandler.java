package com.otpbank.idms.mq.errorhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;

public class MQErrorHandler implements ErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(MQErrorHandler.class);

    @Override
    public void handleError(Throwable throwable) {
        logger.error("Unhanlded exception : " + throwable);
    }

}
