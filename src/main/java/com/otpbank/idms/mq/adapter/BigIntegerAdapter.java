package com.otpbank.idms.mq.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigInteger;

public class BigIntegerAdapter extends XmlAdapter<String, BigInteger> {

    @Override
    public String marshal(BigInteger v) throws Exception {
        if (v != null) {
            return v.toString();
        }
        return null;
    }

    @Override
    public BigInteger unmarshal(String v) throws Exception {
        try {
            return new BigInteger(v);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
