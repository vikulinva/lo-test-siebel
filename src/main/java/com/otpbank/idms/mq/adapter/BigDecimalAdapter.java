package com.otpbank.idms.mq.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigDecimal;

public class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

    @Override
    public String marshal(BigDecimal v) throws Exception {
        if (v != null) {
            return v.toString();
        }
        return null;
    }

    @Override
    public BigDecimal unmarshal(String v) throws Exception {
        try {
            return new BigDecimal(v);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
