package com.otpbank.idms.mq.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DoubleAdapter extends XmlAdapter<String, Double> {

    @Override
    public Double unmarshal(String v) throws Exception {
        try {
            return new Double(v);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public String marshal(Double v) throws Exception {
        if (v != null) {
            return v.toString();
        }
        return null;
    }
}
