package com.otpbank.idms.util;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Created by NikitushkinVG on 08.02.2017.
 */
public class PasswordCipher {
    public String decode(String encoded) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("AES");
        String decoded = new String(cipher.doFinal(DatatypeConverter.parseHexBinary(encoded)));
        return decoded.substring(0, decoded.length());
    }
}
