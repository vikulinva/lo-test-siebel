@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(value = com.otpbank.idms.mq.adapter.BigDecimalAdapter.class, type = java.math.BigDecimal.class),
        @XmlJavaTypeAdapter(value = com.otpbank.idms.mq.adapter.BigIntegerAdapter.class, type = java.math.BigInteger.class),
        @XmlJavaTypeAdapter(value = com.otpbank.idms.mq.adapter.DoubleAdapter.class, type = Double.class)
})
package com.otpbank.idms.pos.model.message;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;